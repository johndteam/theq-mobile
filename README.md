# The Q - Mobile App 

> All the quotes in one place.

![npm](https://img.shields.io/badge/npm-5.6.0-green.svg)
![release](https://img.shields.io/badge/release-0.0.1-blue.svg)

The purpose of the Q app is to manage quotes in a very easy way.

## Usage example

A few motivating and useful examples of how your product can be used. Spice this up with code blocks and potentially more screenshots.

_For more examples and usage, please refer to the [Wiki](https://bitbucket.org/johndteam/theq-mobile/wiki/Home)._

## Development setup

- Clone repo:

```sh
git clone git@bitbucket.org:johndteam/theq-mobile.git
```

- Install dependencies:

```sh
yarn install
```

- Run project:

```sh
yarn start
```

- Choose the emulator:
```sh
i
```
or

```sh
a
```
## Release History

- 0.0.1
     - App Init Done

## Meta

Ivan Tsud – [@JStr00per](https://twitter.com/JStr00per) – ivts@tuta.io

Distributed under the MIT license. See `LICENSE` for more information.

[https://bitbucket.org/ivatsu/](https://bitbucket.org/ivatsu/)

## Acknowledgments

- Hat tip to anyone whose code was used
