module.exports = {
  extends: [
    "airbnb",
    "plugin:prettier/recommended",
    "prettier/flowtype",
    "prettier/react",
    "prettier/standard"
  ],
  plugins: ["prettier-eslint"],
  env: {
    es6: true,
    browser: true,
    jest: true
  },
  parser: "babel-eslint",
  rules: {
    "prettier/prettier": ["error", { singleQuote: true }],
    "react/jsx-filename-extension": [
      1,
      {
        extensions: [".js", ".jsx", "tsx", "ts"]
      }
    ],
    "no-unused-vars": [2, { vars: "local", args: "none" }],
    "no-console": 0,
    "import/prefer-default-export": 0,
    "jsx-a11y/label-has-for": [
      2,
      {
        components: ["Label"],
        required: {
          some: ["nesting", "id"]
        },
        allowChildren: false
      }
    ],
    "no-underscore-dangle": ["error", { allowAfterThis: true }],
    "no-tabs": 0,
    "react/jsx-indent": 0,
    "react/jsx-indent-props": 0,
    "no-mixed-spaces-and-tabs": 0,
    "import/no-extraneous-dependencies": 0,
    "import/extensions": ["ignorePackages"],
    "import/no-unresolved": [2, { ignore: ["@static/images"] }]
  }
};
